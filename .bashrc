# ~/.bashrc

### Test for an interactive shell.
if [[ $- != *i* ]]; then
	return
fi

test -f $HOME/.dircolors && eval $(dircolors -b $HOME/.dircolors)

for f in $HOME/.bash_completion.d/*; do
	source $f
done

for f in $HOME/.bashrc.d/*; do
	source $f
done

shopt -s cmdhist
shopt -s histappend

stty -ixon

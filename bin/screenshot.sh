#!/usr/bin/env bash
TIMESTAMP="$(date +%Y%m%d-%H%M%S)"
FILENAME="${HOME}/Screenshots/ss_${TIMESTAMP}.png"

scrot "${FILENAME}" --select

if [[ $1 == "clipboard" ]]; then
	sleep 0.5 && xclip -sel c -t image/png < ${FILENAME}
fi

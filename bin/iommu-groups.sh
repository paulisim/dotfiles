#!/usr/bin/env bash
shopt -s nullglob

for d in /sys/kernel/iommu_groups/*/devices/*; do
	n=${d#*/iommu_groups/*}; n=${n%%/*}
	printf 'IOMMU %02d ' "$n"
	lspci -nns "${d##*/}"
done

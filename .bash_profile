# ~/.bash_profile
[[ -e $HOME/.bashrc ]] && source $HOME/.bashrc
[[ -e $HOME/.bash_alias ]] && source $HOME/.bash_alias
[[ -e $HOME/.bash_local ]] && source $HOME/.bash_local

export XDG_CONFIG_HOME="$HOME/.config"

if test -z "${XDG_RUNTIME_DIR}"; then
	export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
	if ! test -d "${XDG_RUNTIME_DIR}"; then
		mkdir "${XDG_RUNTIME_DIR}"
		chmod 0700 "${XDG_RUNTIME_DIR}"
	fi
fi

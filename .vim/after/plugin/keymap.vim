" imap <F2> <C-O><F2>
imap <F3> <ESC>:set number!<CR>
imap <F5> <C-O><F5>
imap <F6> <C-O><F6>
imap <F7> <C-O><F7>
imap <F8> <C-O><F8>
" imap <F11> <ESC><Plug>NERDCommenterToggle<CR>ki
" imap <F12> <ESC><F12>

" nmap <F2> :w<CR>
nmap <F3> :set number!<CR>
nmap <F5> :set list listchars=eol:$,tab:\¦->,trail:~,extends:>,precedes:<<CR>
nmap <F6> :set list listchars=eol:$,tab:\¦->,trail:~,extends:>,precedes:<,space:␣<CR>
nmap <F7> :set invlist list?<CR>
nmap <F8> :StripWhitespace<CR>
" nmap <F9> :sort<CR>
" nmap <F11> <Plug>NERDCommenterToggle<CR>k
" nmap <F12> :q!<CR>

" vmap <F9> :sort<CR>
" vmap <F11> <Plug>NERDCommenterToggle<CR>k

noremap <c-u> :r !uuidgen<CR>

" nnoremap <esc><esc><esc> :nohls<CR>
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <silent> <A-Left> :bp<CR>
nnoremap <silent> <A-Right> :bn<CR>

nmap <Leader>cc <Plug>NERDCommenterToggle<CR>k
nmap <Leader>cv <Plug>NERDCommenterAlignLeft<CR>k
nmap <Leader>cb <Plug>NERDCommenterSexy<CR>k
nmap <Leader>e <ESC>:e!<CR>
nmap <Leader>l <ESC>:ls!<CR>
nmap <Leader>qw <ESC>:x!<CR>
nmap <Leader>qq <ESC>:q!<CR>
nmap <Leader>w <ESC>:w<CR>
nmap <Leader>ws <ESC>:w !sudo tee %<CR>

vmap <Leader>cc <Plug>NERDCommenterToggle<CR>k
vmap <Leader>cv <Plug>NERDCommenterAlignLeft<CR>k
vmap <Leader>cb <Plug>NERDCommenterSexy<CR>k
vmap <Leader>s :sort<CR>

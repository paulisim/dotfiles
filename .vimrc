set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'VundleVim/Vundle.vim'
Plugin 'dense-analysis/ale'
Plugin 'hashivim/vim-terraform'
Plugin 'lifepillar/vim-solarized8'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'pearofducks/ansible-vim'
Plugin 'preservim/nerdcommenter'
Plugin 'stephpy/vim-yaml'
Plugin 'tpope/vim-markdown'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin 'vimwiki/vimwiki'
call vundle#end()

set background=dark
set cindent
set cinwords=if,else,while,do,for,switch,case
set clipboard=
set cmdheight=2
set cursorcolumn
set cursorline
set enc=utf-8
set fenc=utf-8
set fileencodings=utf8
set foldmethod=marker
set hidden
set history=500
set is hlsearch ai ic scs
set laststatus=2
set lazyredraw
set list!
" set list listchars=eol:↩,tab:\¦->,trail:~,extends:>,precedes:<
set list listchars=eol:$,tab:\¦->,trail:~,extends:>,precedes:<
set matchpairs=(:),[:],{:},<:>
set modeline
set modelines=5
set noautoindent
set nocompatible
set noswapfile
set nowrap
set number
set ruler
set smarttab
set shortmess=at
set showcmd
set t_ut=
set termencoding=utf8
set timeout timeoutlen=1500
set viminfo='0,f0,\"0,:50,h
set visualbell t_vb=
set wildmenu

if exists('+termguicolors')
	let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
	set termguicolors
endif

filetype plugin on
filetype plugin indent on

" let g:one_allow_italics = 1

let mapleader = ","

let g:NERDCommentEmptyLines = 0
let g:NERDCompactSexyComs = 0
let g:NERDCustomDelimiters = {'text':{'left':'#','right':''}}
let g:NERDSpaceDelims = 1
let g:NERDToggleCheckAllLines = 0
let g:NERDTrimTrailingWhitespace = 1

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_alt_sep = '•'
" let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

let g:airline#extensions#ale#enabled = 1
let g:airline_detect_spell = 0
let g:airline_detect_spelllang = 0
let g:airline_statusline_ontop = 0
let g:airline_symbols_ascii = 1
let g:airline_highlighting_cache = 1

let g:ansible_yamlKeyName = 'yamlKey'
let g:ansible_unindent_after_newline = 1
let g:ansible_template_syntaxes = { '*.j2': 'ruby' }

let g:better_whitespace_ctermcolor = 'none'
let g:better_whitespace_guicolor = 'NONE'

let g:netrw_home = $HOME.'/.vim'

let g:terraform_align = 1
let g:terraform_fmt_on_save = 1

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰']
let g:ale_hover_cursor = 1
let g:ale_hover_to_preview = 1
let g:ale_set_balloons = 1
let g:ale_sign_column_always = 0
let g:ale_sign_error = '»»'
let g:ale_sign_warning = '››'

syntax on

colorscheme solarized8

au BufEnter * if &filetype == "" | setlocal ft=text | endif
au BufEnter,BufNewFile,BufRead *.pp set ft=puppet
au BufWinLeave * call clearmatches()

" https://vimhelp.org/change.txt.html#fo-table
" :5verbose set fo?
" :5verbose setl fo?
au! BufEnter * setlocal fo-=crot textwidth=0

au FileType xml setlocal et sts=2 sw=2 ts=2
au FileType yaml setlocal indentkeys-=<:>
au FileType yaml.ansible setlocal indentkeys-=<:>

hi Comment cterm=italic ctermbg=none guibg=NONE
hi ExtraWhitespace ctermbg=none ctermbg=none ctermfg=red guibg=NONE guifg=lightred
hi MatchParen cterm=bold ctermbg=none ctermfg=red guibg=NONE guifg=red
hi NonText cterm=none ctermbg=none ctermbg=none ctermfg=red guibg=NONE guifg=SeaGreen
hi Specialkey cterm=none ctermbg=none ctermfg=red guibg=NONE
" hi Specialkey cterm=none ctermbg=none ctermfg=red guibg=NONE guifg=#993c3c

autocmd BufNewFile main.yml 0r ~/.vim/skeleton/ansible.yml
autocmd BufNewFile playbook.yml 0r ~/.vim/skeleton/ansible.yml
